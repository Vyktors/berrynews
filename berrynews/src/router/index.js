import Vue from "vue";
import VueRouter from "vue-router";

//**PAGES**

import Home from "@/pages/home/Index.vue";
import Article from "@/pages/article/Index.vue";

Vue.use(VueRouter);

//**PATHS**
const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/article/:id",
    name: "article",
    component: Article
  },
  {
    path: "/github",
    beforeEnter() {
      location.href = "http://github.com/Vyktors";
    }
  },
  {
    path: "/gitlab",
    beforeEnter() {
      location.href = "http://gitlab.com/Vyktors";
    }
  }
];

const router = new VueRouter({
  routes,
  mode: "history"
});

export default router;
