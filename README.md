# BerryNews

Exercice pratique

Cours universitaire `Technologies web avancées H2020` - `8INF349`

## Project setup

```cmd
npm install
```

### Compiles and hot-reloads for development

```cmd
npm run serve
```

### Compiles and minifies for production

```cmd
npm run build
```

### Lints and fixes files

```cmd
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Commentaires

À noter que je ne savais si on pouvais utiliser des components externes comme alors ils sont omis dans la branche principale:

- [Vue-breakpoints](https://github.com/apertureless/vue-breakpoints)
- [Vue-lazy-image](https://github.com/alexjoverm/v-lazy-image)

J'utilise cependant le component `vue-moment` pour l'affichage de la date dans les commentaires.

- [Vue-moment](https://github.com/brockpetrie/vue-moment)

## Contexte

Développer un site de news sur les technologies : **Berry News**. Le plan consiste, non pas d'avoir un seul article, mais d’en avoir 7 nouveaux, pour un total de 8. Pour bien les présenter, chaque article aura sa propre page. La page d’accueil exposera tous les articles du site, du plus récent au plus vieux.

### Plan global

![alt text][plan_global]

### Plan de la page Home (Index)

![plan_index]

### Plan d'un article

![plan_article]

### Plan d'un thumbnail

![plan_thumbnail]

[plan_global]: ressources/plan_global.png "Plan global"
[plan_article]: ressources/plan_article.png "Plan d'un article"
[plan_index]: ressources/plan_index.png "Plan de l'index"
[plan_thumbnail]: ressources/plan_thumbnail.png "Plan d'un thumbnail"
